const fetch = require('node-fetch')

const url = "http://www.jamix.fi/ruokalistapalvelu/rest/haku/menu/97090/1?lang=fi"
const weekDays = [
  "Sunnuntai",
  "Maanantai",
  "Tiistai",
  "Keskiviikko",
  "Torstai",
  "Perjantai",
  "Lauantai",
]

// Found from STACKOVERFLOW
// Finds a key in an object (recursively) and returns its value, example: 'days'
const findVal = (object, key) => {
  var value
  Object.keys(object).some(function(k) {
    if (k === key) {
      value = object[k]
      return true
    }
    if (object[k] && typeof object[k] === 'object') {
      value = findVal(object[k], key)
      return value !== undefined
    }
  })
  return value
}

const getDateString = (date, daynr) => {
  const dd = date.slice(6,8)
  const mm = date.slice(4,6)
  const dayName = weekDays[daynr]
  return `${dayName} (${dd}-${mm})`
}

const formatData = (days) => {
  days = days.sort((a, b) => b.date < a.date)
  let resModified = []

  days.forEach(day => {
    resModified = resModified.concat(
      {
        dateString: getDateString(day.date.toString(), day.weekday),
        ...day
      }
    )
  })
  return resModified
}

const fetchMenu = async () => {
  let res = await fetch(url).then(res => res.json())
  //res = await res.json()
  const days = findVal(res, 'days')
  const resultFormatted = formatData(days)

  // Use this to see FULL print of the menu, to see what's in there
  // console.log(JSON.stringify(resultFormatted, null, 2))
  return resultFormatted
}

// Both 'forEach' functions will be replaced by 'map' functions in react native
// And console.logs replaced by components
const run = async () => {
  const menu = await fetchMenu()
  
  menu.forEach(day => {
    console.log(day.dateString)
    console.log(" ")

    day.mealoptions.forEach(meal => {
      console.log("  " + meal.name + " (" + meal.menuItems[0].diets + ")")
      console.log("    " + meal.menuItems[0].ingredients)
      console.log("")
    })
    console.log(" ")
    console.log("______________________________________________________________________________")
    console.log(" ")
  })
}

run()
